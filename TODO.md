# TODO list

Add todo list helps me to remember what to do ;) !

- [ ] [README.md](./README.md)
  - [ ] Add badges
    - [ ] Python version
    - [ ] Pypi version
    - [ ] Licence MIT
    - [ ] test build
    - [ ] code coverage
- [ ] setup.py
  - [ ] package finder or add manually package ?
  - [ ] The Package name may not yet be final.
- [ ] [CONTRIBUTING.md](CONTRIBUTING.md)
  - [ ] Inspire to https://github.com/pallets/flask/blob/master/CONTRIBUTING.rst
- [ ] Markdown python module
  - [ ] Read usage
  - [ ] Read Extension API usage
- [ ] [makefile](./makefile)
  - [ ] test section
  - [ ] clean section
  - [ ] Add autohelp from Grafikart - make a snippet or gist
  - [ ] build documentation
- [ ] Documentation builder
  - [ ] Read Sphynx
  - [ ] Read MkDocs
- [ ] How to make CLI
  - [ ] https://github.com/Python-Markdown/markdown/blob/a991ed40fb700fac3dd37de4d8095529c9b980b3/setup.py#L53
  - [ ] Rebuild [__main__.py](./mocodo-md/__main__.py)
- [ ] Update Header docstring with [HEADER_DOCSTRING.py](./HEADER_DOCSTRING.py)
- [ ] 