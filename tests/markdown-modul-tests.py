import markdown

def read(filename):
    """
    Simplify read file
    """
    with open(filename, "r") as fh:
        return fh.read()

def basics_trial(filename):
    md = read(filename)
    html = markdown.markdown(md)
    print(html)


if __name__ == "__main__":
    # basics_trial("./MD-files/TEST.md")

    basics_trial("./MD-files/MOCODO-SIMPLE.md")