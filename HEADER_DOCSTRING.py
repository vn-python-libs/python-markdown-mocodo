#!/usr/bin/env python
"""
Mocodo-MD


Mocodo Md is a plugin that allows you to use blocks of Mocodo code of Markdown Notebooks.

Documentation:
GitHub:
GitLab: 
PyPI: https://pypi.org/project/

Didier Bröska <didier.broska@gmail.com>

Copyright 2020 Didier Bröska

License: MIT (see LICENSE.md for details).
"""
